import React from 'react';

import * as SettingsActions from '../actions/SettingsActions';
import SettingsStore from '../stores/SettingsStore';

export default class Settings extends React.Component {
  state = {
    title: SettingsStore.getTitle(),
    color: SettingsStore.getColor()
  };

  onTitleChange = (e) => {
    SettingsActions.setTitle(e.target.value);
  };

  onColorChange = (e) => {
    SettingsActions.setColor(e.target.value);
  };

  getSettings = () => {
    this.setState({
      title: SettingsStore.getTitle(),
      color: SettingsStore.getColor()
    });
  };

  componentDidMount() {
    SettingsStore.onChange(this.getSettings);
  }

  componentWillUnmount() {
    SettingsStore.offChange(this.getSettings);
  }

  render() {
    return (
      <form class="mui-panel">
        <legend>Settings</legend>
        <div class="mui-textfield mui-textfield--float-label">
          <input type="text" value={this.state.title} onChange={this.onTitleChange}/>
          <label>Title</label>
        </div>
        <div class="mui-select">
          <select value={this.state.color} onChange={this.onColorChange} onWheel={this.onColorChange}>
            <option value="">Default</option>
            <option value="red">Red</option>
            <option value="indigo">Indigo</option>
            <option value="green">Green</option>
            <option value="orange">Orange</option>
          </select>
          <label>Color</label>
        </div>
      </form>
    );
  }
}
