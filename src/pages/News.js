import React from 'react';

import * as NewsActions from '../actions/NewsActions';
import NewsStore from '../stores/NewsStore';
import NewsArticle from '../components/NewsArticle';

export default class News extends React.Component {
  state = { articles: NewsStore.getArticles() };

  getArticles = () => {
    this.setState({ articles: NewsStore.getArticles() });
  };

  componentDidMount() {
    NewsStore.onChange(this.getArticles);
    NewsActions.reload();
  }

  componentWillUnmount() {
    NewsStore.offChange(this.getArticles);
  }

  render() {
    return (
      <div class="mui-panel">
        <h1>News</h1>
        <hr class="mui-divider"/>
        {this.state.articles.map((article, i) =>
          <NewsArticle key={i} index={i} data={article}/>
        )}
      </div>
    );
  }
}
