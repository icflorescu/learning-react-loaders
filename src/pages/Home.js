import React from 'react';

export default class Home extends React.Component {
  render() {
    return (
      <div class="mui-panel">
        <h1>Learning React</h1>
        <hr class="mui-divider"/>
        <p>React rulz, Angular sux.</p>
        <p>With React I can haz hot reloading.</p>
        <p>You can haz hot reloading with Angular too, but it's a bit more complicated... as everything else is.</p>
        <p>Also, with React therz less codez.</p>
      </div>
    );
  }
}
