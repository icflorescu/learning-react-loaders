import StoreBase from '../etc/StoreBase';

import dispatcher from '../dispatcher';

class SettingsStore extends StoreBase {
  title = '';
  color = '';

  handleActions(action) {
    switch (action.type) {
      case 'SET_APP_TITLE': {
        this.title = action.title;
        this.emit('change');
        break;
      }
      case 'SET_APP_COLOR': {
        this.color = action.color;
        this.emit('change');
        break;
      }
    }
  }

  getTitle() { return this.title; }
  getColor() { return this.color; }
}

const store = new SettingsStore;
dispatcher.register(store.handleActions.bind(store));

export default store;
