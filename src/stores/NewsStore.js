import StoreBase from '../etc/StoreBase';
import axios from 'axios';

import dispatcher from '../dispatcher';

const NEWS_URL = 'https://newsapi.org/v1/articles?source=techcrunch&apiKey=a861d7370fd74ebea6dd33b34654871a';

class NewsStore extends StoreBase {
  constructor() {
    super();
    this.articles = [];
  }

  load() {
    axios.get(NEWS_URL).then(res => {
      this.articles = res.data.articles;
      this.emit('change');
    });
  }

  handleActions(action) {
    if (action.type == 'RELOAD_NEWS') {
      this.load();
    }
  }

  getArticles() { return this.articles; }
}

const store = new NewsStore;
dispatcher.register(store.handleActions.bind(store));

export default store;
