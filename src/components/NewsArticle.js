import React from 'react';

import './NewsArticle.scss';

export default class NewsArticle extends React.Component {
  render() {
    const data = this.props.data,
          className = this.props.index == 0 ? null : 'mui--divider-top';
    return (
      <div class={className}>
        <h2>
          <a href={data.url} target="_blank">{data.title}</a>
        </h2>
        <p><em>by {data.author}, at {data.publishedAt.toString()}</em></p>
        <img class="news-article-image" src={data.urlToImage}/>
        <p>{data.description}</p>
      </div>
    );
  }
}
