import React from 'react';

import './Footer.scss';

export default class Footer extends React.Component {
  render() {
    return (
      <footer class="footer">
        <div class="mui-container mui--text-center">
          Made with ♥ by <a target="_blank" href="https://linkedin.com/in/icflorescu">Ionuț-Cristian Florescu</a>
        </div>
      </footer>
    );
  }
}
