import React from 'react';

import Footer from './Footer';
import Header from './Header';

import './Layout.scss';

export default class Layout extends React.Component {
  render() {
    return (
      <section class="layout">
        <Header/>
        <div class="content-wrapper">
          <div class="mui--appbar-height"></div>
          <br/>
          <div class="mui-container">
            {this.props.children}
          </div>
        </div>
        <Footer/>
      </section>
    );
  }
}
