import React from 'react';
import { Link } from 'react-router';

import './Header.scss';

import SettingsStore from '../../stores/SettingsStore';

const DEFAULT_TITLE = 'Learning React';

export default class Header extends React.Component {
  state = { title: SettingsStore.getTitle() || DEFAULT_TITLE };

  getSettings = () => {
    this.setState({ title: SettingsStore.getTitle() || DEFAULT_TITLE });
  };

  componentDidMount() {
    SettingsStore.onChange(this.getSettings);
  }

  componentWillUnmount() {
    SettingsStore.offChange(this.getSettings);
  }

  render() {
    return (
      <header class="mui-appbar mui--z1 header">
        <div class="header-container mui-container mui--appbar-height">
          <div class="header-title-container">
            <Link
              class="header-link mui--appbar-line-height"
              to="/"
            >{this.state.title}</Link>
          </div>
          <Link
            class="header-link header-menu-link mui--appbar-line-height"
            activeClassName="header-menu-link-active"
            to="/news"
          >News</Link>
          <Link
            class="header-link header-menu-link mui--appbar-line-height"
            activeClassName="header-menu-link-active"
            to="/settings"
          >Settings</Link>
        </div>
      </header>
    );
  }
}
