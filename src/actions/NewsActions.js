import dispatcher from '../dispatcher';

export function reload(title) {
  dispatcher.dispatch({ type: 'RELOAD_NEWS', title });
}
