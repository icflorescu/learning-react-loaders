import dispatcher from '../dispatcher';

export function setTitle(title) {
  dispatcher.dispatch({ type: 'SET_APP_TITLE', title });
}

export function setColor(color) {
  dispatcher.dispatch({ type: 'SET_APP_COLOR', color });
}
