import { EventEmitter } from 'events';

export default class StoreBase extends EventEmitter {
  onChange(fn) {
    this.on('change', fn);
  }

  offChange(fn) {
    this.removeListener('change', fn);
  }
}
