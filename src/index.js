import './index.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

import Layout from './components/layout/Layout';
import Home from './pages/Home';
import News from './pages/News';
import Settings from './pages/Settings';

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={Layout}>
      <IndexRoute component={Home}/>
      <Route path="/news" component={News}/>
      <Route path="/settings" component={Settings}/>
    </Route>
  </Router>,
  document.getElementById('app')
);
