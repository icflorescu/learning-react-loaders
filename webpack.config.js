const path              = require('path'),
      webpack           = require('webpack'),
      autoprefixer      = require('autoprefixer'),
      ExtractTextPlugin = require('extract-text-webpack-plugin'),
      HtmlWebpackPlugin = require('html-webpack-plugin');

const debug = process.argv.some(item => item.includes('webpack-dev-server'));

const basePlugins = [
  new webpack.optimize.DedupePlugin(),
  new webpack.optimize.OccurenceOrderPlugin(),
  new HtmlWebpackPlugin({ template: 'index.html', minify: { html5: true, collapseWhitespace: true } })
];

module.exports = {
  debug,
  context: path.join(__dirname, 'src'),
  devtool: debug ? 'inline-sourcemap' : null,
  entry: './index.js',
  module: {
    loaders: [{
      test: /\.jsx?$/, exclude: /node_modules/,
      loader:
        (debug ? 'react-hot!' : '') +
        'babel?' + JSON.stringify({
          presets: ['react', 'es2015', 'stage-2'],
          plugins: ['react-html-attrs']
        })
    }, {
      test: /\.scss$/,
      loader: debug ?
        'style!css?sourceMap!postcss?sourceMap!sass?sourceMap' :
        ExtractTextPlugin.extract('style', 'css?minimize!postcss!sass')
    }]
  },
  postcss: [ autoprefixer({ browsers: ['> 5%'] }) ],
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.js'
  },
  plugins: debug ? basePlugins : basePlugins.concat([
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify('production') } }),
    new webpack.optimize.UglifyJsPlugin({ compress: { warnings: false }, output: { comments: false } }),
    new ExtractTextPlugin('index.css')
  ])
};
